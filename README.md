main commands:
**spacing is not important
**the keys are merely for readability of json input,
**  only the first key which holds the main command matters 



	
	exit the program: (very inefficient!!)
		{"do":"exit"}
		0


    
	
	
	
	
	
	
	making new table:
		{
		"newTable" : "table name",
		"columns" : [column1, column2, column3, ...]
		}
		0
    
	
	
	
	
	
	
	
	
	reseting an existing table: (emptying it)
		{"reset":"table name"}
		0
	
    
    
	
	
	
	
	
	
	
	adding to existing table:
		{
		"addTo" : "table name",
		"row" : [column1, column2, column3, ...],
		"row" : [column1, column2, column3, ...],
		.
		.
		.
		"row" : [column1, column2, column3, ...]
		}
		0
    
	
	
	
    
	
	
	
    
    searching in a table:
        two ways for searching:
            1- searches the entire table:
				{
				"searchIn" : "table name",
				"subject" : "what you seek"
				}
				0
            
            2- searches only one specific column/ like among company names:
				{
				"searchIn" : "table name",
				"subject" : "what you seek",
				"category" : "like -->company"  (this is the name of one column in the table)
				}
				0
			
			
			
			
			
			
			
			
			
			
	updating table (to change some specific information in a table):
		{
		"updateIn":"table name",
		"find":"some thing",  (gives access to all rows that contain "some thing" in one specific column[below])
		"inColumn":"column name for searching",
		"put":"some new thing",
		"inColumn":"target column name"
		}
		0
		
		EXAMPLE:
		
			table file name = individuals.txt
					[row]   first name          last name          
					 1      Reza                Fathi
					 2      Amin                Kalami
			(case: say, we know that Mr.Kalami's first name in inserted somehow mistakenly, and we wnat to correct it)		 
			
			{"updateIn":"individuals",
			"find" :"Kalami ",
			"inColumn":"last name",
			"put": "Farhad",
			"inColumn":"first name"}
			0
			
			result:
					[row]   first name          last name          
					 1      Reza                Fathi
					 2      Farhad              Kalami
			
		
		
	
	
	
	
	
	
	
	
	
	delete commands:
		1- delete table completely:   ("delete")
			{
			"delete":"table name"
			}
			0
			
		2- delete specific rows(rows containing specific info):    ("deleteIn")
			
					*****NOTICE*****
					(for instance delete any row that has "Fathi" & "Kazemi" & "Davoodi" in its "last name" column)
						{
						"deleteIn":"table name",
						"rows"    :[T1, T2, T3, ...],
						"inColumn":"column name"
						}
						0
						
			EXAMPLE:
			table file: Students.txt
		
		[row]   frist name                    last name                     id code                       major                         
		 1      Reza                          Fathi                         98222073                      CS                            
		 2      Amir                          Noori                         98222045                      CS                            
		 3      Arman                         Kazemi                        98222001                      CS                            
		 4      Afshin                        Jalili                        98222011                      CS                            
		 5      Arman                         Davoodi                       98222023                      CS                            
		 6      Mohammad Javad                Allame                        98222079                      CS                            
	
		{"deletIn":"Students",
		"rows"    :[1, 2, 3, 4],
		"inColumn":"row"}
		0
		
		result:
		[row]   frist name                    last name                     id code                       major                         		
		 1      Arman                         Davoodi                       98222023                      CS                            
		 2      Mohammad Javad                Allame                        98222079                      CS                            
			
		again...
		
		{"deletIn":"Students",
		"rows"    :[Allame, Davoodi],
		"inColumn":"last name"}
		0
		
		result:
		[row]   frist name                    last name                     id code                       major                         	



		
			
			
[mini DataBase - Reza Fathi]		